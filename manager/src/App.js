import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
    componentWillMoun() {
        const config = {
            apiKey: 'AIzaSyB0PSWePzMNKbiTBXnmNomzOxRLvqrWCZk',
            authDomain: 'manager-8c275.firebaseapp.com',
            databaseURL: 'https://manager-8c275.firebaseio.com',
            projectId: 'manager-8c275',
            storageBucket: 'manager-8c275.appspot.com',
            messagingSenderId: '67916650601'
        };
        firebase.initializeApp(config);
    }
    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk)); // second is option
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
