//expect one creator now
//action wrap by function and this is action creator
export const selectLibrary = (libraryId) => {
  return {
      type: 'select_library',
      payload: libraryId
  };
};